CREATE DATABASE IF NOT EXISTS springboot_jdbc_thymeleaf;

CREATE TABLE subscribers (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE KEY,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO subscribers (firstName, lastName, email) VALUES ("Roland", "Foucher", "rfoucher@gmail.com");
INSERT INTO subscribers (firstName, lastName, email) VALUES ("Jean", "DeLaFontaine", "jdlFontaine@gmail.com");
INSERT INTO subscribers (firstName, lastName, email) VALUES ("Sherlock ", "Holmes", "s.holmes@gmail.com");