package com.simplon.springbootjdbcthymeleaf;



import com.simplon.springbootjdbcthymeleaf.model.Subscriber;
import com.simplon.springbootjdbcthymeleaf.repository.SubscriberRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJdbcThymeleafApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(SpringbootJdbcThymeleafApplication.class, args);
		
	}

}
