package com.simplon.springbootjdbcthymeleaf.controller;

import com.simplon.springbootjdbcthymeleaf.model.Subscriber;
import com.simplon.springbootjdbcthymeleaf.repository.ISubsciberAlt;
import com.simplon.springbootjdbcthymeleaf.repository.ISubscriberRepository;
import com.simplon.springbootjdbcthymeleaf.repository.ITestGlobalRepo;
import com.simplon.springbootjdbcthymeleaf.repository.TestGlobalRepo;
import com.simplon.springbootjdbcthymeleaf.repository.TestSubscriberAlt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;





@Controller
public class SubscriberController {

    
    @Autowired
    private ISubscriberRepository subscriberRepository;
    private TestSubscriberAlt subscriberAlt;
    

    public SubscriberController(TestSubscriberAlt subscriberAlt) {
        this.subscriberAlt = new TestSubscriberAlt();
    }

    @GetMapping("/")
    public String showIndexPageAndGetAllSubscribersString(Model model) {
        model.addAttribute("subscribers", subscriberAlt.findAll());
        
        return "index";
    }

    @GetMapping("/new")
    public String getAddPage(Model model){
        model.addAttribute("subscriber", new Subscriber());
        return "add";
    }

    @PostMapping("/add")
    public String addSubscriber(@ModelAttribute Subscriber subscriber){
        subscriberRepository.add(subscriber);
        return "redirect:/";
    }

    @GetMapping("/get/{id}")
    public ModelAndView getSubscriberById(@PathVariable(name = "id") int id){
        
        ModelAndView modelAndView= new ModelAndView("get");
        Subscriber subscriber = subscriberAlt.findById(id);
        modelAndView.addObject("subscriber", subscriber);
        return modelAndView;
    }

    @GetMapping("/edit/{id}")
    public ModelAndView showEditForm(@PathVariable(name = "id") int id){
        ModelAndView modelAndView = new ModelAndView("edit");
        Subscriber subscriber = subscriberRepository.getSubscriberById(id);
        modelAndView.addObject("subscriber", subscriber);
        return modelAndView;
    }

    @PostMapping("/update")
    public String updateSubscriber(@ModelAttribute("subscriber") Subscriber subscriber){
        subscriberRepository.update(subscriber);
        return "redirect:/";
    }

    @GetMapping(value="/delete/{id}")
    public String deleteSubscriber(@PathVariable(name = "id") int id) {
        subscriberRepository.deleteById(id);
        return "redirect:/";
    }
    


}
