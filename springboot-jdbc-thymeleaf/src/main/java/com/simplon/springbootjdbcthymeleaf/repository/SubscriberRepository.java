package com.simplon.springbootjdbcthymeleaf.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.simplon.springbootjdbcthymeleaf.model.Subscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SubscriberRepository implements ISubscriberRepository {

    private Connection connection;

    private final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";
    private final String GET_SUBSCRIBERS_BY_ID = "SELECT * FROM subscribers WHERE id=?";
    private final String ADD_SUBSCRIBERS = "INSERT INTO subscribers (firstName, lastName, email) VALUES (?,?,?)";
    private final String UPDATE_SUBSCRIBERS = "UPDATE subscribers SET firstName = ?, lastName = ?, email = ? WHERE id = ?";
    private final String DELETE_SUBSCRIBERS = "DELETE FROM subscribers WHERE id=?";

    @Autowired
    private DataSource dataSource;

    

    @Override
    public List<Subscriber> findAll() {

        List<Subscriber> list = new ArrayList<>();
        
        try {
            connection = dataSource.getConnection();
            
            PreparedStatement stmt = connection.prepareStatement(GET_SUBSCRIBERS);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Subscriber subscriber = new Subscriber(result.getInt("id"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getString("email"),
                        result.getTimestamp("createdAt"));

                list.add(subscriber);
            }
            connection.close();
            return list;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            e.getMessage();
        }

        return null;
    }

    public Connection getConnection() {
        return connection;
    }
    
    @Override
    public Integer add(Subscriber subscriber) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(ADD_SUBSCRIBERS, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, subscriber.getFirstName());
            stmt.setString(2, subscriber.getLastName());
            stmt.setString(3, subscriber.getEmail());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                subscriber.setSubscriberId(result.getInt(1));
                connection.close();
                return subscriber.getSubscriberId();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Subscriber getSubscriberById(Integer subscriberId) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(GET_SUBSCRIBERS_BY_ID);
            stmt.setInt(1, subscriberId);
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return new Subscriber(resultSet.getInt("id"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("email"),
                        resultSet.getTimestamp("createdAt"));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean update(Subscriber subscriber) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(UPDATE_SUBSCRIBERS);
            stmt.setString(1, subscriber.getFirstName());
            stmt.setString(2, subscriber.getLastName());
            stmt.setString(3, subscriber.getEmail());
            stmt.setInt(4, subscriber.getSubscriberId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement(DELETE_SUBSCRIBERS);
            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    public void setConnection() {
        try {
            this.connection = dataSource.getConnection();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
