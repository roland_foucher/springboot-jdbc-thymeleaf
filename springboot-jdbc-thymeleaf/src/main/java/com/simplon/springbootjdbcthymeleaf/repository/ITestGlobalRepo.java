package com.simplon.springbootjdbcthymeleaf.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

@Repository
public interface ITestGlobalRepo <T>  {
    T findById(Integer id);

}
