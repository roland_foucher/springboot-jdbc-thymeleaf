package com.simplon.springbootjdbcthymeleaf.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import javax.xml.crypto.Data;

import com.simplon.springbootjdbcthymeleaf.model.Subscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public class TestSubscriberAlt extends TestGlobalRepo <Subscriber> implements ISubsciberAlt{

    private final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";
    public TestSubscriberAlt() {
        this.getByIdQuery = "SELECT * FROM subscribers WHERE id=?";
        
    }
    
   

    @Override
    protected Subscriber instanciateType(ResultSet result) {
        
        try {
            return new Subscriber(result.getInt("id"),
            result.getString("firstName"),
            result.getString("lastName"),
            result.getString("email"),
            result.getTimestamp("createdAt"));
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Subscriber> findAll() {

        List<Subscriber> list = new ArrayList<>();
        
        try {
            connection = dataSource.getConnection();
            
            PreparedStatement stmt = connection.prepareStatement(GET_SUBSCRIBERS);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Subscriber subscriber = new Subscriber(result.getInt("id"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getString("email"),
                        result.getTimestamp("createdAt"));

                list.add(subscriber);
            }
            connection.close();
            return list;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            e.getMessage();
        }

        return null;
    }





    
    
    
}
