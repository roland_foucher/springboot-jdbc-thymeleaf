package com.simplon.springbootjdbcthymeleaf.repository;

import java.util.List;

import com.simplon.springbootjdbcthymeleaf.model.Subscriber;

import org.springframework.boot.autoconfigure.amqp.RabbitProperties.Cache.Connection;


public interface ISubscriberRepository {
    
    List <Subscriber> findAll();
    
    Integer add(Subscriber subscriber);
    Subscriber getSubscriberById(Integer subscriberId);
    boolean update(Subscriber subscriber);
    boolean deleteById(Integer id);
}
