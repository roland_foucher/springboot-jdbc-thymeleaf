package com.simplon.springbootjdbcthymeleaf.repository;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JdbcConfiguration {
    
    public DataSource getDataSource()
    {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("com.mysql.cj.jdbc.Driver");
        dataSourceBuilder.url("jdbc:mysql://localhost:3306/springboot_jdbc_thymeleaf");
        dataSourceBuilder.username("simplon");
        dataSourceBuilder.password("1234");
        return dataSourceBuilder.build();
    }
}

