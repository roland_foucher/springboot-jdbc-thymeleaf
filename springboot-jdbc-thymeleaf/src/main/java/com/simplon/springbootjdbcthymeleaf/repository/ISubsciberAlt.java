package com.simplon.springbootjdbcthymeleaf.repository;

import java.util.List;

import com.simplon.springbootjdbcthymeleaf.model.Subscriber;

import org.springframework.stereotype.Repository;

@Repository
public interface ISubsciberAlt {
    
    List <Subscriber> findAll();
}
