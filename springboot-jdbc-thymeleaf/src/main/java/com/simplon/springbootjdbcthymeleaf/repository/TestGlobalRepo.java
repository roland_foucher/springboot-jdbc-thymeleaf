package com.simplon.springbootjdbcthymeleaf.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;


public class TestGlobalRepo <T> implements ITestGlobalRepo <T> {
    protected Connection connection;
    protected String getQuery; 
    protected String getByIdQuery; 
    protected String addQuery; 
    protected String updateQuery; 
    protected String deleteQuery;
    protected JdbcConfiguration jdbcConfig = new JdbcConfiguration();
    protected DataSource dataSource;

    public TestGlobalRepo() {
        this.dataSource = jdbcConfig.getDataSource();
    }

    public T findById(Integer id){


        try {
            this.setConnection();
            PreparedStatement stmt = connection.prepareStatement(getByIdQuery);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return instanciateType(result);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    protected T instanciateType(ResultSet result) {
        return null;      
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection() {
        try {
            this.connection = dataSource.getConnection() ;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    
}
