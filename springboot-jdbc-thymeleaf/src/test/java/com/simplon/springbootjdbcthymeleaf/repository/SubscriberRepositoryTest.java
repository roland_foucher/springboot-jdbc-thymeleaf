package com.simplon.springbootjdbcthymeleaf.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.List;

import com.simplon.springbootjdbcthymeleaf.model.Subscriber;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


public class SubscriberRepositoryTest {

    private SubscriberRepository subscriberAlt;


    @BeforeEach
    public void setUp() {
       subscriberAlt = new SubscriberRepository();

        try {
            subscriberAlt.setConnection();
            subscriberAlt.getConnection().setAutoCommit(false);
           
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void tearDown() {
        try {
            //Après chaque test on "rollback" la base de données, c'est à dire qu'on
            //annule les modifications qu'on a possiblement faites dessus
            subscriberAlt.getConnection().rollback();
            subscriberAlt.getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    @Test
    void testFindById() {
        Subscriber subscriber = subscriberAlt.getSubscriberById(2);
        assertEquals(2, subscriber.getSubscriberId());
        
    }

    
}
